#include "Math.hpp"
#include <math.h>

namespace Bread
{
    const float Math::PI = 3.141592654f;
    const float Math::DEG_TO_RAD = Math::PI/180.0f;
    const float Math::RAD_TO_DEG = 1.0f/Math::DEG_TO_RAD;

    float Math::distance2D(float x1, float y1, float x2, float y2)
	{
		return sqrt((x2-x1) * (x2-x1) + (y2-y1) * (y2-y1));
	}

	bool Math::collidePointRect(float px, float py, float rx, float ry, float rw, float rh)
	{
		return (px >= rx && px <= rx+rw && py >= ry && py <= ry+rh);
	}
}
