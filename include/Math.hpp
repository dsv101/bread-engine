#ifndef BREAD_MATH_HPP
#define BREAD_MATH_HPP

#include <math.h>
#include <cmath>

namespace Bread
{
    class Math
    {
        public:
            static const float PI;
            static const float DEG_TO_RAD;
            static const float RAD_TO_DEG;
			static float distance2D(float x1, float y1, float x2, float y2);
			static bool collidePointRect(float px, float py, float rx, float ry, float rw, float rh);
    };
}

#endif // BREAD_MATH_HPP
