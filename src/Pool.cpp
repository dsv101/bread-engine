#include "Pool.hpp"

namespace Bread
{
	Pool::Pool() :
		lastPool(nullptr),
		entities()
	{
		
	}
	
	Pool::~Pool()
	{
		
	}
	
	void Pool::addEntity(Entity * e)
	{
		for (Entity * ent : entities)
			if (e == ent)
				return;
				
		entities.push_back(e);
	}
	
	void Pool::removeEntity(Entity * e)
	{
		for (std::vector<Entity*>::iterator it = entities.begin(); it != entities.end(); ++it)
			if ((*it) == e)
			{
				entities.erase(it);
				break;
			}
	}

	void Pool::removeAllEntities()
	{
		entities.clear();
	}
	
	void Pool::addGraphic(Graphic * g)
	{
		for (Graphic * gr : graphics)
			if (g == gr)
				return;
				
		graphics.push_back(g);
	}
	
	void Pool::removeGraphic(Graphic * g)
	{
		for (std::vector<Graphic*>::iterator it = graphics.begin(); it != graphics.end(); ++it)
			if ((*it) == g)
			{
				graphics.erase(it);
				break;
			}
	}

	void Pool::removeAllGraphics()
	{
		graphics.clear();
	}
	
	void Pool::update()
	{
		for (Graphic * g : graphics)
			if (g->isActive())
				g->update();

		for (Entity * e : entities)
			if (e->isActive())
				e->update();
	}
	
	void Pool::render()
	{
		for (Graphic * g : graphics)
			if (g->isVisible())
				g->render();

		for (Entity * e : entities)
			if (e->isVisible())
				e->render();
	}
}
