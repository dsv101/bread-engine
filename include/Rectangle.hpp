#ifndef BREAD_RECTANGLE_HPP
#define BREAD_RECTANGLE_HPP

#include "Bread.hpp"

namespace Bread
{
    class Rectangle
    {
        public:
            Rectangle();
            Rectangle(float x, float y, float w, float h);
            Rectangle(float x, float y, float v);
            Rectangle(const Vector2f& position, const Vector2f& size);
            Rectangle(const Vector2f& position, float w, float h);
            Rectangle(const Vector2f& position, float v);
            Rectangle(float x, float y, const Vector2f& size);

            virtual ~Rectangle();

            void copy(const Rectangle& rectangle);

            void convertNormal();

            Rectangle getNormal() const;

            float getPerimeter() const;
            float getArea() const;

            float x;
            float y;
            float w;
            float h;
    };
}

#endif // BREAD_RECTANGLE_HPP
