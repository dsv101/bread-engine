#ifndef BREAD_UTILS_HPP
#define BREAD_UTILS_HPP

#include "Bread.hpp"

namespace Bread
{
	class Utils
	{
		public:
			static std::string directory;
			static std::string loadFile(const char * fname);
			static std::string ltrim(const char * s);
			static std::string rtrim(const char * s);
			static std::string trim(const char * s);
	};
}

#endif
