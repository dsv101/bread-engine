#ifndef BREAD_TRANSFORMABLE_HPP
#define BREAD_TRANSFORMABLE_HPP

#include "Bread.hpp"
#include "Transform.hpp"

namespace Bread
{
    class Transformable
    {
        public:
            Transformable();
            virtual ~Transformable();

            void setPosition(float x, float y);
            void setPosition(const Vector2f& position);
            void setScale(float x, float y);
            void setScale(const Vector2f& scale);
            void setOrigin(float x, float y);
            void setOrigin(const Vector2f& origin);
            void setRotation(float angle);

            void move(float x, float y);
            void move(const Vector2f& offset);
            void rescale(float x, float y);
            void rescale(const Vector2f& scale);
            void rotate(float angle);

            const Vector2f& getPosition() const;
            const Vector2f& getScale() const;
            const Vector2f& getOrigin() const;
            float getRotation() const;
            const Transform& getTransform() const;
            const Transform& getTransformInverse() const;

        protected:
            void update();

            Vector2f position;
            Vector2f scale;
            Vector2f origin;
            float rotation;
            mutable Transform transform;
            mutable Transform transformInverse;
            mutable bool transformUpdate;
            mutable bool transformInverseUpdate;
    };
}

#endif // BREAD_TRANSFORMABLE_HPP
