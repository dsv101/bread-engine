#ifndef BREAD_ENTITY_HPP
#define BREAD_ENTITY_HPP

#include "Bread.hpp"
#include "Transformable.hpp"
#include "Graphic.hpp"

namespace Bread
{
	class Entity : public Bread::Transformable
	{
		typedef Transformable super;
		
		public:
			Entity(int x, int y, int width, int height, int originX, int originY);
			Entity(Vector2i position, Vector2i size, Vector2i origin);
			Entity();
			virtual ~Entity();
			
			virtual void update();
			virtual void render();
			
			void addGraphic(Graphic * g);
			void removeGraphic(Graphic * g);
			
			void setSize(int width, int height);
			void setSize(Vector2i size);
			
			bool isColliding(Entity * e);
			bool isVisible();
			bool isActive();
			
		protected:
			bool active;
			bool visible;
			
			float width;
			float height;
			
			std::vector<Graphic*> graphics;
	};
}

#endif
