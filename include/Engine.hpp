#ifndef BREAD_ENGINE_HPP
#define BREAD_ENGINE_HPP

#include "Bread.hpp"

#define MAX_RENDER_LAYERS 50000

namespace Bread
{
	class Engine
	{
		public:
			Engine(unsigned int width, unsigned int height, const char* title="Bread Game");
			virtual ~Engine();

			int run();
			void setPool(Pool * p);
			
			void setWindowProperties(const int width, const int height, const char *title);

			static double elapsed;
			
		protected:
			bool alive;
			bool paused;

			SDL_Window * window;
			SDL_Surface * windowSurface;
			SDL_GLContext mainGLContext;
			
			Pool * activePool;
			
			void shutdown();
			virtual void update();
			virtual void render();
	};
}

#endif
