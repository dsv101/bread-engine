#ifndef BREAD_MIXER_HPP
#define BREAD_MIXER_HPP

#include "Bread.hpp"
#include <map>

namespace Bread
{
	class Mixer
	{
		public:
			static std::string directory;

			static void init();
			static void loadSFX(const char *sname, const char *fname);
			static void playSFX(const char *sname, int channel=-1, int loops=0);
			static void setVolume(int channel=-1, int volume=MIX_MAX_VOLUME);
			static void killChannel(int channel=-1);
			static void destroySFX(const char *sname);
			static void destroyAllSFX();

		private:
			static std::map<const char*, Mix_Chunk*> SFXs;
	};
}

#endif