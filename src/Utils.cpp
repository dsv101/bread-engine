#include "Utils.hpp"
#include <fstream>

namespace Bread
{
	std::string Utils::directory = "./";

	std::string Utils::loadFile(const char * fname)
	{
		std::string fn = std::string(directory + std::string(fname)).c_str();

		std::string s = "";
		std::ifstream fs(fn.c_str(), std::ios::in);
		
		if (!fs.is_open())
		{
			LOG(ERROR) << "File: '" << fn << "' not found!" << std::endl;
			return s;
		}
		
		std::string line = "";
		while (!fs.eof())
		{
			std::getline(fs, line);
			s += line + "\n";
		}
		
		fs.close();
		return s;
	}

	std::string Utils::ltrim(const char * s)
	{
		std::string str(s);
		std::string whitespaces (" \t\f\v\n\r");

		std::size_t found = str.find_first_not_of(whitespaces);
		if (found!=std::string::npos)
			str.erase(0,found);
		else
			str.clear();

		return str;
	}

	std::string Utils::rtrim(const char * s)
	{
		std::string str(s);
		std::string whitespaces (" \t\f\v\n\r");

		std::size_t found = str.find_last_not_of(whitespaces);
		if (found!=std::string::npos)
			str.erase(found+1);
		else
			str.clear();

		return str;
	}

	std::string Utils::trim(const char * s)
	{
		return ltrim(rtrim(s).c_str());
	}
}
