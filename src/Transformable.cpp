#include "Transformable.hpp"

namespace Bread
{
    Transformable::Transformable() :
        position(0,0),
        scale(1,1),
        origin(0,0),
        rotation(0),
        transform(),
        transformInverse(),
        transformUpdate(false),
        transformInverseUpdate(false)
    {

    }

    Transformable::~Transformable()
    {

    }

    void Transformable::update()
    {
        transformUpdate = true;
        transformInverseUpdate = true;
    }

    void Transformable::setPosition(float x, float y)
    {
        position.x = x;
        position.y = y;
        update();
    }

    void Transformable::setPosition(const Vector2f& position)
    {
        setPosition(position.x, position.y);
    }

    void Transformable::setScale(float x, float y)
    {
        scale.x = x;
        scale.y = y;
        update();
    }

    void Transformable::setScale(const Vector2f& scale)
    {
        setScale(scale.x, scale.y);
    }

    void Transformable::setOrigin(float x, float y)
    {
        origin.x = x;
        origin.y = y;
        update();
    }

    void Transformable::setOrigin(const Vector2f& origin)
    {
        setOrigin(origin);
    }

    void Transformable::setRotation(float angle)
    {
        rotation = fmod(static_cast<float>(angle), 360.0f);
        if (rotation < 0.0f)
        {
            rotation += 360.0f;
        }
        update();
    }

    void Transformable::move(float x, float y)
    {
        setPosition(position.x+x, position.y+y);
    }

    void Transformable::move(const Vector2f& offset)
    {
        setPosition(position.x+offset.x, position.y+offset.y);
    }

    void Transformable::rescale(float x, float y)
    {
        setScale(scale.x*x, scale.y*y);
    }

    void Transformable::rescale(const Vector2f& scale)
    {
        setScale(this->scale.x*scale.x, this->scale.y*scale.y);
    }

    void Transformable::rotate(float angle)
    {
        setRotation(rotation+angle);
    }

    const Vector2f& Transformable::getPosition() const
    {
        return position;
    }

    const Vector2f& Transformable::getScale() const
    {
        return scale;
    }

    const Vector2f& Transformable::getOrigin() const
    {
        return origin;
    }

    float Transformable::getRotation() const
    {
        return rotation;
    }

    const Transform& Transformable::getTransform() const
    {
        if (transformUpdate)
        {
            float angle = -rotation*Math::DEG_TO_RAD;
            float cosine = cosf(angle);
            float sine = sinf(angle);
            float cosineScaleX = cosine*scale.x;
            float cosineScaleY = cosine*scale.y;
            float sineScaleX = sine*scale.x;
            float sineScaleY = sine*scale.y;
            float positionX = (cosineScaleX*origin.x)-(sineScaleY*origin.y)+position.x;
            float positionY = (sineScaleX*origin.x)-(cosineScaleY*origin.y)+position.y;
            transform = Transform(cosineScaleX,sineScaleY,position.x,-sineScaleX,cosineScaleY,position.y,0.0f,0.0f,1.0f);
            transformUpdate = false;
        }
        return transform;
    }

    const Transform& Transformable::getTransformInverse() const
    {
        if (transformInverseUpdate)
        {
            transformInverse = getTransform().getInverse();
            transformInverseUpdate = false;
        }
        return transformInverse;
    }
}
