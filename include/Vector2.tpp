
template <typename T>
inline Vector2<T>::Vector2() :
    x(0),
    y(0)
{

}

template <typename T>
inline Vector2<T>::Vector2(T v) :
    x(v),
    y(v)
{

}

template <typename T>
inline Vector2<T>::Vector2(T x, T y) :
    x(x),
    y(y)
{

}

template <typename T>
template <typename U>
inline Vector2<T>::Vector2(const Vector2<U>& vector) :
    x(static_cast<T>(vector.x)),
    y(static_cast<T>(vector.y))
{

}

template <typename T>
template <typename U>
inline void Vector2<T>::copy(const Vector2<U>& vector)
{
    x = static_cast<T>(vector.x);
    y = static_cast<T>(vector.y);
}

template <typename T>
inline void Vector2<T>::convertPolar()
{
    copy(getPolar());
}

template <typename T>
inline void Vector2<T>::convertPositional()
{
    copy(getPositional());
}

template <typename T>
inline void Vector2<T>::convertNormal()
{
    copy(getNormal());
}

template <typename T>
Vector2<T> Vector2<T>::getPolar() const
{
    return Vector2<T>(sqrt((x*x)+(y*y)),atan2(y,x));
}

template <typename T>
Vector2<T> Vector2<T>::getPositional() const
{
    return Vector2<T>(x*cos(y),x*sin(y));
}

template <typename T>
Vector2<T> Vector2<T>::getNormal() const
{
    return Vector2<T>(-y,x);
}

template <typename T>
inline T Vector2<T>::getDotProduct(const Vector2<T>& vector) const
{
    return (x*vector.x)+(y*vector.y);
}

template <typename T>
inline Vector2<T> operator -(const Vector2<T>& right)
{
    return Vector2<T>(-right.x,-right.y);
}

template <typename T>
inline Vector2<T> operator +(const Vector2<T>& left, T right)
{
    return Vector2<T>(left.x+right,left.y+right);
}

template <typename T>
inline Vector2<T> operator +(T left, const Vector2<T>& right)
{
    return Vector2<T>(left+right.x,left+right.y);
}

template <typename T>
inline Vector2<T> operator +(const Vector2<T>& left, const Vector2<T>& right)
{
    return Vector2<T>(left.x+right.x,left.x+right.x);
}

template <typename T>
inline Vector2<T>& operator +=(Vector2<T>& left, const Vector2<T>& right)
{
    left.x += right.x;
    left.y += right.y;
    return left;
}

template <typename T>
inline Vector2<T>& operator +=(Vector2<T>& left, T right)
{
    left.x += right;
    left.y += right;
    return left;
}

template <typename T>
inline Vector2<T> operator -(const Vector2<T>& left, T right)
{
    return Vector2<T>(left.x-right,left.y-right);
}

template <typename T>
inline Vector2<T> operator -(T left, const Vector2<T>& right)
{
    return Vector2<T>(left-right.x,left-right.y);
}

template <typename T>
inline Vector2<T> operator -(const Vector2<T>& left, const Vector2<T>& right)
{
    return Vector2<T>(left.x-right.x,left.y-right.y);
}

template <typename T>
inline Vector2<T>& operator -=(Vector2<T>& left, const Vector2<T>& right)
{
    left.x -= right.x;
    left.y -= right.y;
    return left;
}

template <typename T>
inline Vector2<T>& operator -=(Vector2<T>& left, T right)
{
    left.x -= right;
    left.y -= right;
    return left;
}

template <typename T>
inline Vector2<T> operator *(const Vector2<T>& left, T right)
{
    return Vector2<T>(left.x*right,left.y*right);
}

template <typename T>
inline Vector2<T> operator *(T left, const Vector2<T>& right)
{
    return Vector2<T>(left*right.x,left*right.y);
}

template <typename T>
inline Vector2<T> operator *(const Vector2<T>& left, const Vector2<T>& right)
{
    return Vector2<T>(left.x*right.x,left.y*right.y);
}

template <typename T>
inline Vector2<T>& operator *=(Vector2<T>& left, const Vector2<T>& right)
{
    left.x *= right.x;
    left.y *= right.x;
    return left;
}

template <typename T>
inline Vector2<T>& operator *=(Vector2<T>& left, T right)
{
    left.x *= right;
    left.y *= right;
    return left;
}

template <typename T>
inline Vector2<T> operator /(const Vector2<T>& left, T right)
{
    return Vector2<T>(left.x/right,left.y/right);
}

template <typename T>
inline Vector2<T> operator /(T left, const Vector2<T>& right)
{
    return Vector2<T>(right.x/left,right.y/left);
}

template <typename T>
inline Vector2<T> operator /(const Vector2<T>& left, const Vector2<T>& right)
{
    return Vector2<T>(left.x/right.x,left.y/right.y);
}

template <typename T>
inline Vector2<T>& operator /=(Vector2<T>& left, const Vector2<T>& right)
{
    left.x /= right.x;
    left.y /= right.y;
    return left;
}

template <typename T>
inline Vector2<T>& operator /=(Vector2<T>& left, T right)
{
    left.x /= right;
    left.y /= right;
    return left;
}

template <typename T>
inline bool operator ==(const Vector2<T>& left, const Vector2<T>& right)
{
    if ((left.x != right.x) || (left.y != right.y))
    {
        return false;
    }
    return true;
}

template <typename T>
inline bool operator !=(const Vector2<T>& left, const Vector2<T>& right)
{
    if ((left.x == right.x) && (left.y == right.y))
    {
        return false;
    }
    return true;
}
