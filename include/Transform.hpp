#ifndef BREAD_TRANSFORM_HPP
#define BREAD_TRANSFORM_HPP

#include "Bread.hpp"

namespace Bread
{
    class Transform
    {
        public:
            Transform();
            Transform(float v0_0, float v0_1, float v0_2, float v1_0, float v1_1, float v1_2, float v2_0, float v2_1, float v2_2);
            virtual ~Transform();

            const float* getMatrix() const;

            float getDeterminant() const;

            Transform getInverse() const;

            Vector2f transformPoint(float x, float y) const;
            Vector2f transformPoint(const Vector2f& point) const;

            static const Transform IDENTITY;

        protected:
            float matrix[16];
    };
}

#endif // BREAD_TRANSFORM_HPP
