#include "Engine.hpp"
#include "Input.hpp"
#include "Mixer.hpp"
#include "Pool.hpp"

INITIALIZE_EASYLOGGINGPP;

namespace Bread
{
	double Engine::elapsed = 0;
	
	Engine::Engine(unsigned int width, unsigned int height, const char* title) :
		alive(true),
		paused(false),
		window(SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL)),
		activePool(nullptr)
	{
		LOG(INFO) << "Log using default file" << std::endl;

		if (window == NULL)
		{
			LOG(FATAL) << "Failed To Initialize Window: " << SDL_GetError() << std::endl;
			alive = false;
			return;
		}

		windowSurface = SDL_GetWindowSurface(window);
		mainGLContext = SDL_GL_CreateContext(window);
		if(glewInit() != GLEW_OK)
			LOG(ERROR) << "glewInit failed";;
			
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		glClearColor(1, 0.5f, 0.5f, 1);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0, width, height, 0, MAX_RENDER_LAYERS, -1);
		glMatrixMode(GL_MODELVIEW);
		glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		Input::init();
		Mixer::init();
		Graphic::init();
	}

	Engine::~Engine()
	{
		Mixer::destroyAllSFX();
		
		SDL_GL_DeleteContext(mainGLContext);
		
		if (window != NULL)
			SDL_DestroyWindow(window);
		
		SDL_Quit();
	}

	int Engine::run()
	{
		SDL_Event ev;
		int time = 0;
		int lastTime = 0;
		
		while (alive)
		{
			Input::reset();
			
			while (SDL_PollEvent(&ev) != 0)
			{
				switch(ev.type)
				{
					case SDL_QUIT:
						shutdown();
						break;
				}

				Input::update(ev);
			}


			if (!paused)
			{
				time = SDL_GetTicks();
				Engine::elapsed = static_cast<double>(time - lastTime) / 1000.0;
				lastTime = time;
				update();
				
				glClear(GL_COLOR_BUFFER_BIT);
				
				render();
				
				SDL_GL_SwapWindow(window);
			}
		}

		return 0;
	}

	void Engine::setPool(Pool * p)
	{
		if (this->activePool != nullptr)
			p->lastPool = this->activePool;

		this->activePool = p;
	}

	void Engine::setWindowProperties(const int width, const int height, const char *title)
	{
		SDL_SetWindowSize(window, width, height);
		SDL_SetWindowTitle(window, title);
	}

	void Engine::shutdown()
	{
		alive = false;
	}

	void Engine::update()
	{
		if (activePool != nullptr)
			activePool->update();
	}

	void Engine::render()
	{
		if (activePool != nullptr)
			activePool->render();
	}
}
