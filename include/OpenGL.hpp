#ifndef BREAD_OPENGL_HPP
#define BREAD_OPENGL_HPP

#include <Config.hpp>

#if defined(OS_WINDOWS)

    // The Visual C++ version of gl.h uses WINGDIAPI and APIENTRY but doesn't define them
    #ifdef _MSC_VER
        #include <windows.h>
    #endif

    #include <GL/glew.h>
    #include <GL/gl.h>
    #include <GL/glu.h>

#elif defined(OS_LINUX)

    #include <GL/glew.h>
	#include <GL/gl.h>
	#include <GL/glu.h>

#elif defined(OS_MAC)

    #include <OpenGL/glew.h>
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>

#endif

#endif
