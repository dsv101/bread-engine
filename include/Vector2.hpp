#ifndef BREAD_VECTOR_2_HPP
#define BREAD_VECTOR_2_HPP

#include "Math.hpp"

namespace Bread
{
    template <typename T>
    class Vector2
    {
        public:
            Vector2();
            Vector2(T v);
            Vector2(T x, T y);

            template <typename U>
            explicit Vector2(const Vector2<U>& vector);

            virtual ~Vector2() {};

            template <typename U>
            void copy(const Vector2<U>& vector);

            void convertPolar();
            void convertPositional();
            void convertNormal();

            Vector2<T> getPolar() const;
            Vector2<T> getPositional() const;
            Vector2<T> getNormal() const;

            T getDotProduct(const Vector2<T>& vector) const;

            T x;
            T y;
    };

    template <typename T>
    Vector2<T> operator -(const Vector2<T>& right);

    template <typename T>
    Vector2<T> operator +(const Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator +(T left, const Vector2<T>& right);

    template <typename T>
    Vector2<T> operator +(const Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator +=(Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator +=(Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator -(const Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator -(T left, const Vector2<T>& right);

    template <typename T>
    Vector2<T> operator -(const Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator -=(Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator -=(Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator *(const Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator *(T left, const Vector2<T>& right);

    template <typename T>
    Vector2<T> operator *(const Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator *=(Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator *=(Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator /(const Vector2<T>& left, T right);

    template <typename T>
    Vector2<T> operator /(T left, const Vector2<T>& right);

    template <typename T>
    Vector2<T> operator /(const Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator /=(Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    Vector2<T>& operator /=(Vector2<T>& left, T right);

    template <typename T>
    bool operator ==(const Vector2<T>& left, const Vector2<T>& right);

    template <typename T>
    bool operator !=(const Vector2<T>& left, const Vector2<T>& right);

    #include "Vector2.tpp"
}

#endif // BREAD_VECTOR_2_HPP
