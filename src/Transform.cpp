#include "Transform.hpp"

namespace Bread
{
    const Transform Transform::IDENTITY;

    Transform::Transform()
    {
        for (unsigned int i = 0; i != 16; ++i)
        {
            if (i/4 == i%4)
            {
                matrix[i] = 1.0f;
            }
            else
            {
                matrix[i] = 0.0f;
            }
        }
    }

    Transform::Transform(float v0_0, float v0_1, float v0_2, float v1_0, float v1_1, float v1_2, float v2_0, float v2_1, float v2_2)
    {
        matrix[0] = v0_0;
        matrix[1] = v1_0;
        matrix[2] = 0.0f;
        matrix[3] = v2_0;

        matrix[4] = v0_1;
        matrix[5] = v1_1;
        matrix[6] = 0.0f;
        matrix[7] = v2_1;

        matrix[8] = 0.0f;
        matrix[9] = 0.0f;
        matrix[10] = 1.0f;
        matrix[11] = 0.0f;

        matrix[12] = v0_2;
        matrix[13] = v1_2;
        matrix[14] = 0.0f;
        matrix[15] = v2_2;
    }

    Transform::~Transform()
    {

    }

    const float* Transform::getMatrix() const
    {
        return matrix;
    }

    float Transform::getDeterminant() const
    {
        float determinant;
        determinant = matrix[0]*((matrix[15]*matrix[5])-(matrix[7]*matrix[13]));
        determinant -= matrix[1]*((matrix[15]*matrix[4])-(matrix[7]-matrix[12]));
        determinant += matrix[2]*((matrix[13]*matrix[4])-(matrix[5]*matrix[12]));
        return determinant;
    }

    Transform Transform::getInverse() const
    {
        float determinant = getDeterminant();
        if (determinant != 0.0f)
        {
            float v0_0 = ((matrix[15]*matrix[5])-(matrix[7]*matrix[13]))/determinant;
            float v0_1 = ((matrix[15]*matrix[4])-(matrix[7]*matrix[12]))/-determinant;
            float v0_2 = ((matrix[13]*matrix[4])-(matrix[5]*matrix[12]))/determinant;

            float v1_0 = ((matrix[15]*matrix[1])-(matrix[3]*matrix[13]))/-determinant;
            float v1_1 = ((matrix[15]*matrix[0])-(matrix[3]*matrix[12]))/determinant;
            float v1_2 = ((matrix[13]*matrix[0])-(matrix[1]*matrix[12]))/-determinant;

            float v2_0 = ((matrix[7]*matrix[1])-(matrix[3]*matrix[5]))/determinant;
            float v2_1 = ((matrix[7]*matrix[0])-(matrix[3]*matrix[4]))/-determinant;
            float v2_2 = ((matrix[5]*matrix[0])-(matrix[1]*matrix[4]))/determinant;

            return Transform(v0_0, v0_1, v0_2, v1_0, v1_1, v1_2, v2_0, v2_1, v2_2);
        }
        return IDENTITY;
    }

    Vector2f Transform::transformPoint(float x, float y) const
    {
        return Vector2f((matrix[0]*x)+(matrix[4]*y)+matrix[12],(matrix[1]*y)+(matrix[5]*y)+matrix[13]);
    }

    Vector2f Transform::transformPoint(const Vector2f& point) const
    {
        return transformPoint(point.x, point.y);
    }
}
