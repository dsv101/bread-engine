#ifndef BREAD_INPUT_HPP
#define BREAD_INPUT_HPP

#include "Bread.hpp"
#include <map>
#include <vector>

namespace Bread
{
	class Input
	{
		public:
			static int mouseX;
			static int mouseY;

			static void init();
			static void update(SDL_Event event);

			static void define(const char* name, std::vector<int> keys);
			static void reset();

			static bool keyPressed(int key);
			static bool keyReleased(int key);
			static bool keyDown(int key);

			static bool keyPressed(const char* name);
			static bool keyReleased(const char* name);
			static bool keyDown(const char* name);

			static bool mousePressed(Uint8 mb);
			static bool mouseReleased(Uint8 mb);
			static bool mouseDown(Uint8 mb);

		private:
			static std::map<int,bool> keysPressed;
			static std::map<int,bool> keysReleased;
			static std::map<int,bool> keysDown;
			static std::map<const char*, std::vector<int>> userSet;

			static std::map<Uint8,bool> mouseButtonsPressed;
			static std::map<Uint8,bool> mouseButtonsReleased;
			static std::map<Uint8,bool> mouseButtonsDown;
		};
}

#endif
