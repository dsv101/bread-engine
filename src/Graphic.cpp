#include "Graphic.hpp"
#include "Math.hpp"

namespace Bread
{
	std::string Graphic::directory = "./";
	void Graphic::init()
	{
		if (TTF_Init() < 0)
			LOG(ERROR) << "Failed to initialize ttf!" << std::endl;
	}
	
	Graphic::Graphic() :
		super(),
		textureId(0),
		width(0),
		height(0),
		layer(0),
		active(true),
		visible(true)
	{
		
	}
	
	Graphic::~Graphic()
	{
		if (textureId != 0)
			glDeleteTextures(1, &textureId);
	}
	
	void Graphic::update()
	{
		
	}
	
	void Graphic::render()
	{
		if (visible)
			renderRelative(Vector2f(0,0), Vector2f(1,1), Vector2f(0,0), 0);
	}
	
	void Graphic::renderRelative(Vector2f position, Vector2f scale, Vector2f origin, float rotation)
	{
		if (!visible)
			return;

		glPushMatrix();
		
		glTranslatef(position.x + this->position.x, position.y + this->position.y, 0.f);
		glRotatef(rotation + this->rotation, 0.f, 0.f, 1.f);
		glScalef(scale.x * this->scale.x, scale.y * this->scale.y, 1.f);
		glTranslatef(-origin.x - this->origin.x, -origin.y - this->origin.y, 0.f);
		
		glBindTexture(GL_TEXTURE_2D, textureId);
		
		glBegin(GL_QUADS);
		glTexCoord2f(0.f, 0.f);
		glVertex3f(0, 0, layer);
		
		glTexCoord2f(1.f, 0.f);
		glVertex3f(width, 0, layer);
		
		glTexCoord2f(1.f, 1.f);
		glVertex3f(width, height, layer);
		
		glTexCoord2f(0.f, 1.f);
		glVertex3f(0, height, layer);
		glEnd();
		
		glPopMatrix();
	}
	
	bool Graphic::createFromFile(const char * fname)
	{
		std::string path = Graphic::directory + std::string(fname);
		SDL_Surface * s = IMG_Load(path.c_str());
		
		if (s == NULL)
		{
			LOG(ERROR) << "Failed to create texture from '" << path << "' " << std::endl;
			return false;
		}
		
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		int mode = (s->format->BytesPerPixel == 4) ? GL_RGBA : GL_RGB;
		
		glTexImage2D(GL_TEXTURE_2D, 0, mode, s->w, s->h, 0, mode, GL_UNSIGNED_BYTE, s->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		width = s->w;
		height = s->h;
		
		SDL_FreeSurface(s);
		return true;
	}

	bool Graphic::createFromTTF(const char * text, int size, const char * fname, float r, float g, float b, float a)
	{
		std::string path = directory + std::string(fname);
		TTF_Font *font = TTF_OpenFont(path.c_str(),size);

		if (font == NULL)
		{
			LOG(ERROR) << "Failed to create font from '" << path << "' " << std::endl;
			return false;
		}

		SDL_Color color = {(Uint8)(b * 255.0), (Uint8)(g * 255.0), (Uint8)(r * 255.0), (Uint8)(a * 255.0)};
		SDL_Surface * s = TTF_RenderUTF8_Blended(font, text, color);
		
		if (s == NULL)
		{
			LOG(ERROR) << "Failed to create texture from '" << path << "' " << std::endl;
			return false;
		}

		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		int mode = (s->format->BytesPerPixel == 4) ? GL_RGBA : GL_RGB;
		
		glTexImage2D(GL_TEXTURE_2D, 0, mode, s->w, s->h, 0, mode, GL_UNSIGNED_BYTE, s->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		width = s->w;
		height = s->h;
		
		SDL_FreeSurface(s);
		TTF_CloseFont(font);
		return true;
	}
	
	bool Graphic::createFromRect(int w, int h, float r, float g, float b, float a)
	{
		float data[w*h*4];

		for (int i = 0; i < w*h*4; i+=4)
		{
			data[i] = r;
			data[i+1] = g;
			data[i+2] = b;
			data[i+3] = a;
		}
		
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_FLOAT, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		width = w;
		height = h;

		return true;
	}
	
	int Graphic::getWidth() { return width; }
	int Graphic::getHeight() { return height; }
	bool Graphic::isActive() { return active; }
	bool Graphic::isVisible() { return visible; }
	void Graphic::setLayer(int layer) {this->layer = layer;}
	void Graphic::setVisible(bool visible) {this->visible = visible;}
}
