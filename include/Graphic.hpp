#ifndef BREAD_GRAPHIC_HPP
#define BREAD_GRAPHIC_HPP

#include <map>
#include "Bread.hpp"
#include "Transformable.hpp"

namespace Bread
{
	class Graphic : public Transformable
	{
		typedef Transformable super;
		
		public:
			static std::string directory;
			static void init();
			
			Graphic();
			virtual ~Graphic();
			
			virtual void update();
			virtual void render();
			void renderRelative(Vector2f position, Vector2f scale, Vector2f origin, float rotation);
			
			bool createFromFile(const char * fname);
			bool createFromTTF(const char * text, int size, const char * fname, float r = 0.0, float g = 0.0, float b = 0.0, float a = 1.0);
			bool createFromRect(int w, int h, float r = 0.0, float g = 0.0, float b = 0.0, float a = 1.0);
			int getWidth();
			int getHeight();
			
			bool isVisible();
			bool isActive();
			
			void setLayer(int layer);
			void setVisible(bool visible);
			
		protected:
			GLuint textureId;
			int width;
			int height;
			int layer;
			bool visible;
			bool active;
	};
}

#endif
