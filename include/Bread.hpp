#ifndef BREAD_BREAD_HPP
#define BREAD_BREAD_HPP

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include "OpenGL.hpp"
#include "easylogging++.h"
#include "Vector2.hpp"

namespace Bread
{
    class Engine;
	class Pool;
    class Input;
    class Rectangle;
    class Transformable;
    class Transform;
    class Utils;
    class Shader;
    class Graphic;
    class Mixer;

    template <typename T>
    class Vector2;
    typedef Vector2<char> Vector2c;
    typedef Vector2<int> Vector2i;
    typedef Vector2<unsigned int> Vector2u;
    typedef Vector2<float> Vector2f;
    typedef Vector2<double> Vector2d;
}

#endif // BREAD_BREAD_HPP
