#ifndef BREAD_SHADER_HPP
#define BREAD_SHADER_HPP

#include "Bread.hpp"

namespace Bread
{
	class Shader
	{
		public:
			static std::string directory;
			static GLuint load(const char * vertPath, const char * fragPath);
	};
}

#endif
