#include "Engine.hpp"
#include "Input.hpp"
#include "Rectangle.hpp"
#include <string>

int main(int argc, char *argv[])
{
    Bread::Rectangle test(6.0f,7.0f,8.0f,9.0f);
    Bread::Vector2f test1(1.0f,2.0f);
    Bread::Vector2f test2(test1*4.0f);
    Bread::Vector2f test3(0.5f);
    Bread::Vector2f test4(test2*test3);
    Bread::Vector2f test5(3.0f);
    Bread::Vector2f test6(test4*test5);
    std::string str1 = "";
    std::string str2 = "  ";
    Bread::Vector2f& vector1 = test1;
    Bread::Vector2f& vector2 = test2;
    str1 += std::to_string((int)vector1.x)+str2+std::to_string((int)vector1.y)+str2;
    str1 += std::to_string((int)vector2.x)+str2+std::to_string((int)vector2.y)+str2;
    str1 += std::to_string(vector1.getDotProduct(vector2))+str2;
    str1 += std::to_string(test4.getDotProduct(test6))+str2+std::to_string(test.getPerimeter());
    Bread::Engine* engine = new Bread::Engine(440,360,str1.c_str());
    engine->run();
    delete engine;
    return 0;
}
