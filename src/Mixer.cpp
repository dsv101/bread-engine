#include "Mixer.hpp"

namespace Bread
{
	std::string Mixer::directory = "./";
	std::map<const char*, Mix_Chunk*> Mixer::SFXs;

	void Mixer::init()
	{
		if (SDL_Init(SDL_INIT_AUDIO) < 0 || Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
			LOG(ERROR) << "Failed to initialize sound!" << std::endl;

		Mix_AllocateChannels(32);
	}

	void Mixer::loadSFX(const char *sname, const char *fname)
	{
		std::string fn = directory + std::string(fname);

		SFXs[sname] = NULL;
		SFXs[sname] = Mix_LoadWAV(fn.c_str());
		if (SFXs[sname] == NULL)
			LOG(ERROR) << "Failed to load " << fn << std::endl;
	}

	void Mixer::playSFX(const char *sname, int channel, int loops)
	{
		if (SFXs.find(sname) == SFXs.end())
			LOG(ERROR) << "Failed to play sound '" << sname << "' because it does not exist!" << std::endl;
		else
		{
			if (Mix_PlayChannel(channel, SFXs[sname], loops) < 0)
				LOG(ERROR) << "Failed to play sound '" << sname << "'" << std::endl;
		}
	}

	void Mixer::setVolume(int channel, int volume)
	{
		Mix_Volume(channel,volume);
	}

	void Mixer::killChannel(int channel)
	{
		Mix_HaltChannel(channel);
	}

	void Mixer::destroySFX(const char *sname)
	{
		if (SFXs.find(sname) == SFXs.end())
			LOG(WARNING) << "Failed to destroy sound '" << sname << "' because it does not exist!" << std::endl;
		else
			Mix_FreeChunk(SFXs[sname]);
	}
	
	void Mixer::destroyAllSFX()
	{
		for (std::pair<const char*, Mix_Chunk*> s : SFXs)
			destroySFX(s.first);
	}
}