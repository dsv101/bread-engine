#include "Shader.hpp"
#include "Utils.hpp"
#include <vector>

namespace Bread
{
	std::string Shader::directory = "./";

	GLuint Shader::load(const char * vertPath, const char * fragPath)
	{
		std::string vp = std::string(directory + std::string(vertPath)).c_str();
		std::string fp = std::string(directory + std::string(fragPath)).c_str();

		GLuint vs = glCreateShader(GL_VERTEX_SHADER);
		GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
		
		std::string vs__ = Utils::loadFile(vp.c_str()) + "\n";
		std::string fs__ = Utils::loadFile(fp.c_str()) + "\n";
		
		const char * vs_ = vs__.c_str();
		const char * fs_ = fs__.c_str();
		
		GLint result = GL_FALSE;
		int logLength;
		
		glShaderSource(vs, 1, &vs_, NULL);
		glCompileShader(vs);
		
		glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
		glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> vertShaderError((logLength > 1) ? logLength : 1);
		glGetShaderInfoLog(vs, logLength, NULL, &vertShaderError[0]);
		if (logLength > 1)
			LOG(ERROR) << &vertShaderError[0];
		
		glShaderSource(fs, 1, &fs_, NULL);
		glCompileShader(fs);
		
		glGetShaderiv(fs, GL_COMPILE_STATUS, &result);
		glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> fragShaderError((logLength > 1) ? logLength : 1);
		glGetShaderInfoLog(fs, logLength, NULL, &fragShaderError[0]);
		if (logLength > 1)
			LOG(ERROR) << &fragShaderError[0];
		
		GLuint prog = glCreateProgram();
		glAttachShader(prog, vs);
		glAttachShader(prog, fs);
		glLinkProgram(prog);
		
		glGetProgramiv(prog, GL_LINK_STATUS, &result);
		glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logLength);
		std::vector<char> programError((logLength > 1) ? logLength : 1);
		glGetProgramInfoLog(prog, logLength, NULL, &programError[0]);
		if (logLength > 1)
			LOG(ERROR) << &programError[0];
		
		glDeleteShader(vs);
		glDeleteShader(fs);
		
		return prog;
	}
}
