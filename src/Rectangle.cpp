#include "Rectangle.hpp"

namespace Bread
{
    Rectangle::Rectangle() :
        x(0),
        y(0),
        w(0),
        h(0)
    {

    }

    Rectangle::Rectangle(float x, float y, float w, float h) :
        x(x),
        y(y),
        w(w),
        h(h)
    {

    }

    Rectangle::Rectangle(float x, float y, float v) :
        x(x),
        y(y),
        w(v),
        h(v)
    {

    }

    Rectangle::Rectangle(const Vector2f& position, const Vector2f& size) :
        x(position.x),
        y(position.y),
        w(size.x),
        h(size.y)
    {

    }

    Rectangle::Rectangle(const Vector2f& position, float w, float h) :
        x(position.x),
        y(position.y),
        w(w),
        h(h)
    {

    }

    Rectangle::Rectangle(const Vector2f& position, float v) :
        x(position.x),
        y(position.y),
        w(v),
        h(v)
    {

    }

    Rectangle::Rectangle(float x, float y, const Vector2f& size) :
        x(x),
        y(y),
        w(size.x),
        h(size.y)
    {

    }

    Rectangle::~Rectangle()
    {

    }

    void Rectangle::copy(const Rectangle& rectangle)
    {
        x = rectangle.x;
        y = rectangle.y;
        w = rectangle.w;
        h = rectangle.h;
    }

    void Rectangle::convertNormal()
    {
        copy(getNormal());
    }

    Rectangle Rectangle::getNormal() const
    {
        return Rectangle(x/w, y/h, 1.0f, 1.0f);
    }

    float Rectangle::getPerimeter() const
    {
        return (w*2.0f)+(h*2.0f);
    }

    float Rectangle::getArea() const
    {
        return w*h;
    }
}
