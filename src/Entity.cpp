#include "Entity.hpp"

namespace Bread
{
	Entity::Entity(int x, int y, int width, int height, int originX, int originY) :
		super(),
		active(true),
		visible(true)
	{
		setPosition(x, y);
		setSize(width, height);
		setOrigin(originX, originY);
	}
	
	Entity::Entity(Vector2i position, Vector2i size, Vector2i origin) :
		Entity(position.x, position.y, size.x, size.y, origin.x, origin.y)
	{
		
	}
	
	Entity::Entity() :
		Entity(0, 0, 0, 0, 0, 0)
	{
		
	}
	
	Entity::~Entity()
	{
		
	}
	
	void Entity::update()
	{
		
	}
	
	void Entity::render()
	{
		glPushMatrix();
		
		for (Graphic * g : graphics)
			if (visible)
				g->renderRelative(position, scale, origin, rotation);
				
		glPopMatrix();
	}
	
	void Entity::addGraphic(Graphic * g)
	{
		for (Graphic * gr : graphics)
			if (g == gr)
				return;
				
		graphics.push_back(g);
	}
	
	void Entity::removeGraphic(Graphic * g)
	{
		for (std::vector<Graphic*>::iterator it = graphics.begin(); it != graphics.end(); ++it)
			if ((*it) == g)
			{
				graphics.erase(it);
				break;
			}
	}
	
	void Entity::setSize(int width, int height)
	{
		this->width = width;
		this->height = height;
	}
	
	void Entity::setSize(Vector2i size)
	{
		setSize(size.x, size.y);
	}
	
	bool Entity::isColliding(Entity * e)
	{
		//TODO: Implement Me
		
		return false;
	}
	
	bool Entity::isActive() { return active; }
	bool Entity::isVisible() { return visible; }
}
