#include "Input.hpp"

namespace Bread
{	
	std::map<int,bool> Input::keysPressed;
	std::map<int,bool> Input::keysReleased;
	std::map<int,bool> Input::keysDown;
	std::map<const char*, std::vector<int>> Input::userSet;
	
    int Input::mouseX = 0;
    int Input::mouseY = 0;
    std::map<Uint8,bool> Input::mouseButtonsPressed;
    std::map<Uint8,bool> Input::mouseButtonsReleased;
    std::map<Uint8,bool> Input::mouseButtonsDown;

	void Input::init()
	{
		//TODO: Init Joystick and Mouse
	}
	
	void Input::update(SDL_Event event)
	{
		
		if (event.type == SDL_KEYDOWN && event.key.repeat == 0)
        {
            keysPressed[event.key.keysym.sym] = true;
            keysDown[event.key.keysym.sym] = true;
        }
        else if (event.type == SDL_KEYUP && event.key.repeat == 0)
        {
            keysReleased[event.key.keysym.sym] = true;
            keysDown[event.key.keysym.sym] = false;
        }
        else if (event.type == SDL_MOUSEBUTTONDOWN)
        {
            mouseButtonsPressed[event.button.button] = true;
            mouseButtonsDown[event.button.button] = true;
        }
        else if (event.type == SDL_MOUSEBUTTONUP)
        {
            mouseButtonsReleased[event.button.button] = true;
            mouseButtonsDown[event.button.button] = false;
        }

        SDL_GetMouseState(&mouseX, &mouseY);
	}
	
	void Input::define(const char* name, std::vector<int> keys)
    {
        userSet[name] = keys;
    }

    void Input::reset()
    {
        keysPressed.clear();
        keysReleased.clear();

        mouseButtonsPressed.clear();
        mouseButtonsReleased.clear();
    }

    bool Input::keyPressed(int key)
    {
        return (keysPressed[key]);
    }

    bool Input::keyReleased(int key)
    {
        return (keysReleased[key]);
    }

    bool Input::keyDown(int key)
    {
        return (keysDown[key]);
    }

    bool Input::keyPressed(const char* name)
    {
        if (userSet.count(name) > 0)
            for (int key : userSet[name])
                if (keyPressed(key))
                    return true;

        return false;
    }

    bool Input::keyReleased(const char* name)
    {
        if (userSet.count(name) > 0)
            for (int key : userSet[name])
                if (keyReleased(key))
                    return true;

        return false;
    }

    bool Input::keyDown(const char* name)
    {
        if (userSet.count(name) > 0)
            for (int key : userSet[name])
                if (keyDown(key))
                    return true;

        return false;
    }

    bool Input::mousePressed(Uint8 mb)
    {
        return (mouseButtonsPressed[mb]);
    }

    bool Input::mouseReleased(Uint8 mb)
    {
        return (mouseButtonsReleased[mb]);
    }

    bool Input::mouseDown(Uint8 mb)
    {
        return (mouseButtonsDown[mb]);
    }
}
