#ifndef BREAD_POOL_HPP
#define BREAD_POOL_HPP

#include "Bread.hpp"
#include "Entity.hpp"
#include "Graphic.hpp"
#include <vector>

namespace Bread
{
	class Pool
	{
		friend class Engine;
		
		private:
			Pool * lastPool;
			std::vector<Entity*> entities;
			std::vector<Graphic*> graphics;
			
		public:
			Pool();
			virtual ~Pool();
			
			void addEntity(Entity * e);
			void removeEntity(Entity * e);
			void removeAllEntities();
			
			void addGraphic(Graphic * g);
			void removeGraphic(Graphic * g);
			void removeAllGraphics();

			virtual void update();
			virtual void render();
	};
}

#endif
