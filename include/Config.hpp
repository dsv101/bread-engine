#ifndef BREAD_CONFIG_HPP
#define BREAD_CONFIG_HPP

#define BREAD_VERSION_MAJOR 0
#define BREAD_VERSION_MINOR 1

#if defined(OS_WINDOWS)
    #define OS_OVERRIDE
#elif defined(OS_LINUX)
    #define OS_OVERRIDE
#elif defined (OS_OSX)
    #define OS_OVERRIDE
#else
    #if defined(_WIN32)
        #define OS_WINDOWS
        #ifndef NOMINMAX
            #define NOMINMAX
        #endif
    #elif defined(__APPLE__) && defined(__MACH__)
        #include "TargetConditionals.h"
        #if TARGET_OS_MAC
            #define OS_MAC
        #endif
    #elif defined(__linux__)
        #define OS_LINUX
    #else
        #error This operating system is not supported by Bread
    #endif
#endif
#if !defined(NDEBUG)
    #define BREAD_DEBUG
#endif

#endif // BREAD_CONFIG_HPP
